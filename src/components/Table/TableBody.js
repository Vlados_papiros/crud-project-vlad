import React from 'react';
import TableElement from './TableElement';


const TableBody = ({tableData}) => {
    return(
        <tbody>
        {tableData.map((element, index) =>
            <tr key={index}>
                <TableElement element={element}/>
            </tr>
        )}
        </tbody>
    )
}

export default TableBody;
