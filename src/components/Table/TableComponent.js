import React from 'react';
import TableHeaders from "./TableHeaders";
import TableBody from "./TableBody";
import Example from './TableForm/FormAddNewRecord';

class TableComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            headers: []
        };
        this.getHeaders = this.getHeaders.bind(this);
        this.addNewRecord = this.addNewRecord.bind(this);
    }

    getHeaders(element) {
        let cloneHeaders=[];
        Object.keys(element).map(index => (
            cloneHeaders.push(index)
        ))
        this.setState({headers: cloneHeaders})
    }



    componentDidMount() {
        const { tableData } = this.props;
        this.setState({ tableData });
        this.getHeaders(tableData[0]);
    }


    addNewRecord = (record) => {
        const {tableData} = this.state;
        console.log(record);
        const cloneTableData = Object.assign([], tableData);
        cloneTableData.push(record);
        console.log(cloneTableData);
        this.setState({...this.state, tableData: cloneTableData});
    }


    render() {
        const {tableData} = this.state;
        return (
            <div>
                <Example headers={this.state.headers} addNewRecord={this.addNewRecord}/>
                <table>
                    <TableHeaders headers={this.state.headers} />
                    <TableBody tableData={tableData} />
                </table>
            </div>
        )
    }
}

export default TableComponent;