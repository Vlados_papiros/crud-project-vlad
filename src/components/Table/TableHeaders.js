import React from 'react';

const TableHeaders = (props) => {
    return(
        <thead>
        <tr>
            {props.headers.map((element, index) =>
                <th key={index}>{element}</th>
            )}
        </tr>
        </thead>
    )
}

export default TableHeaders;