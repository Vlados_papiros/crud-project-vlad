import React from 'react';

function InputComponent(props) {

    return (
        <div>
            <label htmlFor={props.name}>{props.name}</label>
            <input
                type="text"
                name={props.name}
                onChange={props.handleInputChange}
            />
        </div>
    )
}

export default InputComponent;