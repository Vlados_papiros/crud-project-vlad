import React, { useState, useEffect } from "react"
import Input from './InputComponent';

function FormAddNewRecord(props) {
    const [form, setForm] = useState({});

    const handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        const form1 = form;

        form1[name] =value;

        setForm(form1);
    }

    useEffect(() => {
        const getHeadersForm = () => {
            const cloneHeaders = {};
            props.headers.map((item) => (
                cloneHeaders[item] = ''
            ))
            setForm(cloneHeaders);
        }
        getHeadersForm();

    }, [props.headers]);

    const submitForm = (event) => {
        event.preventDefault();
        props.addNewRecord(form);
    }

    return (
        <form onSubmit={submitForm}>
            {Object.keys(form).map((item,index) => {
                return(<Input
                        key={index}
                        name={item}
                        handleInputChange={handleInputChange}/>
                )
            })}
            <input type="submit" value="Add new record"/>
        </form>
    )
}

export default FormAddNewRecord;