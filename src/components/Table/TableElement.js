import React from 'react';


const TableElement = ({element}) => {
    return(
        Object.keys(element).map(index => (
            <td key={index}>
                {element[index]}
            </td>
        ))
    )
}

export default TableElement;
